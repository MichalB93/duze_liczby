#include "pomoc.h"
#include "ProstaLiczbaProw.h"

	ProstaLiczbaCount::ProstaLiczbaCount(){++ile;}
	ProstaLiczbaCount::~ProstaLiczbaCount(){--ile;}
	unsigned long long ProstaLiczbaCount::GetIle(){return ile;}
	unsigned long long ProstaLiczbaCount::ile=0;

	ProstaLiczba::~ProstaLiczba(){}
	
	ProstaLiczba::ProstaLiczba():liczba(0){}

	int ProstaLiczba::ilecompare=0;
	int ProstaLiczba::ilegetincrementedabs=0;
	int ProstaLiczba::ileinkrementujabs=0;
	int ProstaLiczba::iledekrementujabs=0;
	
	ProstaLiczba::ProstaLiczba( unsigned long long b, bool zn):liczba(b), znak(zn){}
	ProstaLiczba::ProstaLiczba( const ProstaLiczba &ref):liczba(ref.liczba), znak(ref.znak){}


	void ProstaLiczba::display()
	{
		if(znak)
			cout<<'-';
		cout<<liczba<<endl;
	}

	void ProstaLiczba::clear()
	{
		liczba=0;
		znak=0;
	}

	Porownanie ProstaLiczba::compare( const ProstaLiczba &ref)
	{
		++ilecompare;
		if(znak^ref.znak)
		{
			if(znak)
				return mniejszy;
			return wiekszy;
		}
		if(liczba == ref.liczba)
			return rowny;
		if((znak && liczba<ref.liczba) || (!znak && liczba>ref.liczba))
			return wiekszy;
		return mniejszy;
	}

	ProstaLiczba ProstaLiczba::operator= ( const ProstaLiczba &ref)
	{
		liczba=ref.liczba;
		znak=ref.znak;
		return *this;
	}
	
	ProstaLiczba ProstaLiczba::operator+ ( const ProstaLiczba &ref)
	{
		if(znak^ref.znak)
		{
			if(znak)
			{
				//todo
			}
			//todo
		}
		liczba+=ref.liczba;
		return *this;
	}
	
	ProstaLiczba ProstaLiczba::inkrementuj(){if(liczba>=0) ++ileinkrementujabs; else ++iledekrementujabs;++liczba;}

	ProstaLiczba ProstaLiczba::getIncremented(){++ilegetincrementedabs;return liczba+1;}

	ProstaLiczba ProstaLiczba::dekrementuj(){if(liczba>=0)++iledekrementujabs;else ++ileinkrementujabs;--liczba;}