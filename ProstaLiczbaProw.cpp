#include "pomoc.h"
#include "ProstaLiczbaProw.h"

	ProstaLiczbaCount::ProstaLiczbaCount(){++ile;}
	ProstaLiczbaCount::~ProstaLiczbaCount(){--ile;}
	unsigned long long ProstaLiczbaCount::GetIle(){return ile;}
	unsigned long long ProstaLiczbaCount::ile=0;

	ProstaLiczba::~ProstaLiczba(){}
	
	ProstaLiczba::ProstaLiczba():liczba(0){}

	int ProstaLiczba::ilecompare=0;
	int ProstaLiczba::ilegetincrementedabs=0;
	int ProstaLiczba::ileinkrementujabs=0;
	int ProstaLiczba::iledekrementujabs=0;
	
	ProstaLiczba::ProstaLiczba( long long b):liczba(b){}
	ProstaLiczba::ProstaLiczba( const ProstaLiczba &ref):liczba(ref.liczba){}


	void ProstaLiczba::display()
	{
		cout<<liczba<<endl;
	}

	void ProstaLiczba::clear()
	{
		liczba=0;
	}

	Porownanie ProstaLiczba::compare( const ProstaLiczba &ref)
	{
		++ilecompare;
		if(liczba<=ref.liczba)
		{
			if(liczba==ref.liczba)
				return rowny;
			return mniejszy;
		}
		else
			return wiekszy;
	}

	ProstaLiczba ProstaLiczba::operator= ( const ProstaLiczba &ref)
	{
		liczba=ref.liczba;
		return *this;///
	}
	
	ProstaLiczba ProstaLiczba::operator+ ( const ProstaLiczba &ref)
	{
		ProstaLiczba result(liczba+ref.liczba);
		return result;
	}
	
	void ProstaLiczba::inkrementuj(){if(liczba>=0) ++ileinkrementujabs; else ++iledekrementujabs;++liczba;}

	ProstaLiczba ProstaLiczba::getIncremented(){++ilegetincrementedabs;return ProstaLiczba(liczba+1);}

	void ProstaLiczba::dekrementuj(){if(liczba>=0)++iledekrementujabs;else ++ileinkrementujabs;--liczba;}