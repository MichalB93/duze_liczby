#ifndef BLOK_H
#define BLOK_H
#include "pomoc.h"

//#include "Prosta_Liczba_Nowa.h"
//#include "ProstaLiczbaProwpomiedzyTODO.h"
#include "ProstaLiczbaProw.h"

class DuzaLiczba;

class BlokCount{public:
static unsigned long long ile;
BlokCount();//{++ile;}
~BlokCount();//{--ile;}
static unsigned long long getIle();
};


class Blok: BlokCount{public:
	ProstaLiczba wykladnik;
	unsigned Pola[rozmiar_bloku];
	
	Blok();

	Blok(const Blok & bl);

	Blok(const ProstaLiczba &wyk, unsigned u=0);
	
	operator bool();
	
	Porownanie compare(Blok &bl);
	
	void clear(int i);

	void clearBeginning(int i);

	bool empty();

	bool dodaj(const Blok &bl, bool prze);
	
	bool dodajU(unsigned u, int i);
	
	bool dodajJeden(int i=0);
	
	unsigned multiply(unsigned u);
	
	void odejmijJeden(); // Blok mustn't equal zero
	
	bool odejmij(const Blok &bl);//returns false if the block equals zero after substraction

	void parse(const string &str, int i); // string odwrocony

	void parseFraction(const string &str, int i); // string odwrocony

	void display();

	string displayBinarilly();
	
	string displayHexadecimally();

	bool zero();


};

	list<Blok>::iterator operator+(list<Blok>::iterator ite, int a);
#endif