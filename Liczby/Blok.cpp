#include "pomoc.h"
#include "Blok.h"


BlokCount::BlokCount(){++ile;/*cout<<"ILE BLOKOW:"<<ile<<endl;*/}
BlokCount::~BlokCount(){--ile;/*cout<<"ILE BLOKOW:"<<ile<<endl;*/}
unsigned long long BlokCount::getIle()
{
	return ile;
}

unsigned long long BlokCount::ile=0;

/////////////////blok

	Blok::Blok()
	{
		for(int i=0;i<rozmiar_bloku;++i)
		{
			Pola[i]=0;
		}
	}
	Blok::Blok(const Blok & bl)
	{
		wykladnik=bl.wykladnik;
		for(int i=0;i<rozmiar_bloku;++i)
		{
			Pola[i]=bl.Pola[i];
		}
	}
	Blok::Blok(const ProstaLiczba &wyk, unsigned u=0):wykladnik(wyk)
	{
		for(int i=0;i<rozmiar_bloku;++i)
		{
			Pola[i]=u;
		}
	}
	Blok::operator bool()
	{
		return !(this->zero());
	}
	Porownanie Blok::compare(Blok &bl)//also checks the exponent
	{
		Porownanie poro;
		if( (poro=wykladnik.compare(bl.wykladnik))!=rowny )
				return poro;
		for(int i=rozmiar_bloku-1;i>=0;--i)
		{
			if(Pola[i]>bl.Pola[i])
				return wiekszy;
			if(Pola[i]<bl.Pola[i])
				return mniejszy;
		}
		return rowny;
	}
	void Blok::clear(int i=0)// assign zeros to the table of unsigned's starting from index i torwards the end of the table
	{
		for(i;i<rozmiar_bloku;++i)
			Pola[i]=0;
	}
	void Blok::clearBeginning(int i)// assign zeros to the table starting from index i torwards the beginning of the table
	{
		for(i;i>=0;--i)
			Pola[i]=0;
	}
	bool Blok::empty()// return false if all elements of the table are equal to zero
	{
		for(int i=0;i<rozmiar_bloku;++i)
			if(Pola[i])
				return false;
		return true;
	}

	bool Blok::dodaj(const Blok &bl, bool prze)
	{
		unsigned long long pom;
		for(int i=0;i<rozmiar_bloku;++i)
		{
			pom=Pola[i];
			pom+=bl.Pola[i];
			pom+=prze;//cout<<"POM POM  "<<pom<<endl;
			prze=0;
			if(pom>MAX_UNSIGNED)
			{	//cout<<"POM POM DUZE "<<pom<<endl;
				prze=1;
				pom-=MAX_UNSIGNEDijeden;
			}
			Pola[i]=pom;
		}
		return prze;
	}
	
	bool Blok::dodajU(unsigned u, int i)
	{
		unsigned long long pom;
		pom=(unsigned long long)Pola[i]+u;
		if(pom>MAX_UNSIGNED)
		{
			Pola[i]=pom-MAX_UNSIGNEDijeden;
			if(i==(rozmiar_bloku-1))
				return 1;
			return (this->dodajJeden(i+1));
		}
		Pola[i]=pom;
		return 0;
	}
	
	bool Blok::dodajJeden(int i)
	{
		if(Pola[i]<MAX_UNSIGNED)
		{
			++(Pola[i]);return 0;
		}
		Pola[i]=0;
		for( ++i;i<rozmiar_bloku;++i)
		{
			if(Pola[i]<MAX_UNSIGNED)
			{
				++(Pola[i]);return 0;
			}
			Pola[i]=0;
		}
		return 1;
	}
	
	void odejmijJeden() // Blok mustn't equal zero
	{//if(zero()) cout<<"\n\n\nBLAD W ODEJMIJ JEDEN\n\n\n";
		unsigned u=0;
		while(1)
		{
			if(Pola[u])
			{
				--Pola[u];
				return;
			}
			Pola[u]=MAX_UNSIGNED;
		}
	}
	
	void Blok::odejmij(const Blok &bl)//substracts with no regard for the exponent, argument must not be bigger than *this
	{
		for(int i=rozmiar_bloku-1;i>=0;--i)
		{
			Pola[i]==bl.Pola[i];
		}
	}

	void Blok::parse(const string &str, int i) // string odwrocony
	{//cout<<"Parsuje "<<i<<" -"<<str<<endl;
			for(int j=0;j<8;++j)
			{
				Pola[i]<<=4;
				if(str[j]>48&&str[j]<58)
					Pola[i]+=str[j]-48;
				else if(str[j]>96&&str[j]<103)
					Pola[i]+=str[j]-87;
				else if(str[j]>64&&str[j]<71)
					Pola[i]+=str[j]-55;
				else Pola[i]&=0xFFFFFFF0;
			}
	}
	void Blok::parseFraction(const string &str, int i) // string odwrocony
	{//cout<<"Parsuje Fraction "<<i<<" -"<<str<<endl;
			for(int j=0;j<8;++j)
			{
				Pola[i]<<=4;
				switch(str[j])
				{
					//case '1': Pola[i]+=;ZBADAC
				}
				if(str[j]>48&&str[j]<58)
					Pola[i]+=str[j]-48;
				else if(str[j]>96&&str[j]<103)
					Pola[i]+=str[j]-87;
				else if(str[j]>64&&str[j]<71)
					Pola[i]+=str[j]-55;
				else Pola[i]&=0xFFFFFFF0;
			}
	}
	void Blok::display()
	{
		cout<<"--------------------------------------------\nwykladnik:                                 |\n";
		wykladnik.display();
		for(int i=0;i<rozmiar_bloku;++i)
		{
			cout<<Pola[i]<<" ";
		}cout<<"\n--------------------------------------------\n";
	}
	string Blok::displayBinarilly()
	{
		string result;
		for(int i=0;i<rozmiar_bloku;++i)
		{
			result+=displayUBinarilly(Pola[i])+" ";
		}
		return result;
	}
	string Blok::displayHexadecimally()
	{
		string result;
		int j;
		unsigned u, i;
		char c;
		for(i=0;i<rozmiar_bloku;++i)
		{
			u=Pola[i];
			for(j=0;j<8;++j)
			{
				c=u%16;
				if(c<10)
					result+=c+48;
				else
					result+=c+87;
				u=u>>4;
			}
		}
		return result;
	}
	bool Blok::zero()
	{
		for(int i=0;i<rozmiar_bloku;++i)
		{
			if(Pola[i])
				return false;
		}
		return true;
	}
	
	list<Blok>::iterator operator+(list<Blok>::iterator ite, int a)
	{
	if(a==1)
		return ++ite;
	for(int i=0;i<a;++i)
		++ite;
	return ite;
	}
