#include "pomoc.h"
#include "Duze_Liczby.h"

DuzaLiczbaCount::DuzaLiczbaCount(){++ile;}
DuzaLiczbaCount::~DuzaLiczbaCount(){--ile;}
unsigned long long DuzaLiczbaCount::getIle()
{
	return ile;
}

unsigned long long DuzaLiczbaCount::ile=0;
	
	DuzaLiczba::DuzaLiczba():znak(0){}
	DuzaLiczba::DuzaLiczba(const DuzaLiczba &dl)
	{
		znak=dl.znak;
		lista=dl.lista;
	}
	DuzaLiczba::DuzaLiczba(unsigned u)
	{
		znak=0;
		Blok bl;
		bl.Pola[0]=u;
		lista.push_front(bl);
	}
	
	DuzaLiczba::DuzaLiczba(int i)
	{
		Blok bl;
		if(i>=0)
		{
			bl.Pola[0]=i;
			znak=0;
		}
		else
		{
			bl.Pola[0]=-i;
			znak=1;
		}
		lista.push_front(bl);
	}
	
	DuzaLiczba::DuzaLiczba(unsigned long long ull)
	{
		Blok bl;
		unsigned u=ull&0x00000000FFFFFFFF;
		bl.Pola[0]=u;
		u=ull>>32;
		if(rozmiar_bloku==1)
		{
			lista.push_front(bl);
			bl.wykladnik.inkrementuj();
			bl.Pola[0]=u;
			lista.push_back(bl);
			return;
		}
		bl.Pola[1]=u;
		lista.push_front(bl);
	}
	
	DuzaLiczba::DuzaLiczba(double d)
	{//todo
		Blok bl;
	}
	
	bool DuzaLiczba::zero()
	{
		return lista.empty();
	}
	
	DuzaLiczba::operator string()
	{
		string str;
		
		for(list<Blok>::iterator i=lista.begin();i!=lista.end();++i)
		{
			
		}
		return str;
	}
	
	/*DuzaLiczba::operator double()// works only with temporary SimpleNumber
	{
		double result=0;
		int i;
		long long l;
		for(list<Blok>::iterator ite=lista.begin();ite!=lista.end();++ite)
		{
			l=(*ite).wykladnik.liczba;
			for(i=0;i<rozmiar_bloku;++i)
			{
				if(l>0)
				{//cout<<"kurwa l>0 "<<pow(16, (((l-1)*rozmiar_bloku+i)*8))<<endl;
					result+=(double)(pow(16, (((l-1)*rozmiar_bloku+i)*8)))*(*ite).Pola[i];
				}
				else if(l<0)
				{
					//result+=(double)((l-1)*rozmiar_bloku+i)*8*(*ite).Pola[i];
				}
				else
				{//cout<<"kurwa l==0 "<<(i*8)<<" "<<(double)(pow(16, (i*8)))*(*ite).Pola[i]<<endl;
					result+=(double)(pow(16, (i*8)))*(*ite).Pola[i];
				}
			}
		}
		return result;
	}*/
	void DuzaLiczba::display()
	{
		cout<<"----------------------------------------------------------------------\n";
		cout<<"Znak:"<<(znak?"ujemna":"dodatnia")<<endl;
		if(lista.empty())
		{
			cout<<"Zero\n----------------------------------------------------------------------\n";
			return;
		}
		cout<<"Liczba:\n";
		for(list<Blok>::iterator i=lista.begin();i!=lista.end();++i)
		{
			(*i).display();
		}
		cout<<"\n----------------------------------------------------------------------\n";
	}
	string DuzaLiczba::displayBinarilly()//
	{//todo 
		string str;
		if(lista.empty())
		{
			return "0";
		}
		for(list<Blok>::iterator i=lista.begin();i!=lista.end();++i)
		{
			str+=(*i).displayBinarilly();
		}
		return str;
	}
	string DuzaLiczba::displayHexadecimally()//test check
	{
		string result;
		bool zn=znak;
		if(lista.empty())
		{
			return "0";
		}
		if(zn)
			result+='-';
		list<Blok>::iterator i=lista.begin();
		ProstaLiczba wyk=(*i).wykladnik;
		unsigned u;
		for(list<Blok>::iterator i=lista.begin();i!=lista.end();++i)
		{
			wyk.inkrementuj();
			if(zn&&(!wyk.znak))
			{
				result+=',';			// showing a coma where the fractional part ends
				zn=0;
			}
			while((*i).wykladnik.compare(wyk)==2)
			{
				wyk.inkrementuj();
				for(u=rozmiar_bloku;u>0;--u)
				{
					result+="00000000";
				}
			}
			wyk=(*i).wykladnik;
			result+=(*i).displayHexadecimally();
		}
		if(zn)
			result+=",0";
		return result;
	}
	void DuzaLiczba::normalise()//przetrestowac
	{
		for(list<Blok>::iterator ite=lista.begin();ite!=lista.end();)
		{
			if((*ite).zero())
			{
				ite=lista.erase(ite);
			}
			else 
				++ite;
		}
	}
	void DuzaLiczba::parseFromString(const string &str)//zoptymalizować rsrhrtyhjtryjfgvngyjftgy
	{
		lista.clear();
		unsigned long long i;
		if(str[0]=='-')
			{znak=1;i=1;}
		else 
			{znak=0;i=0;}
		unsigned pom=str.find(',');//ewentualnie ","
		if(pom==MAX_UNSIGNED)//check 
		{//cout<<"Parsuje sam integer\n";
			Blok bl;int j;
			if(znak)
			 i=1;
			else
			 i=0;
			string pomoc=str;
			pom=str.length();
			while(1)
			{
				for(j=0;j<rozmiar_bloku;++j)
				{
					if(i+8>=pom)
					{
						bl.clear(j);
						for(int l=i+8-pom;l>0;--l)
							pomoc+=".";
						bl.parse( reverse(substr(pomoc, i, i+8)), j);
						if(!bl.zero())
							lista.push_back(bl);
						return;
					}
					bl.parse( reverse(substr(pomoc, i, i+8)), j);
					i+=8;
				}
				if(!bl.zero())
					lista.push_back(bl);
				bl.wykladnik.inkrementuj();
			}
		}
		else
		{//cout<<"Parsuje integer i fraction\n";
			string fraction, integer;
			Blok bl;int j;bool flaga=1;
			fraction=substr(str, i, pom);//cout<<fraction<<endl;
			integer=substr(str, pom+1, str.length()+1);//cout<<integer<<endl;
			pom=integer.length();
			while(flaga)
			{
				for(j=0;j<rozmiar_bloku;++j)
				{
					if(i+8>=pom)
					{
						bl.clear(j);
						for(int l=i+8-pom;l>0;--l)
							integer+=".";
						bl.parse( reverse(substr(integer, i, i+8)), j);
						if(!bl.zero())
							lista.push_back(bl);
						flaga=0;
						break;
					}
					bl.parse( reverse(substr(integer, i, i+8)), j);
					i+=8;
				}
				if(flaga&&(!bl.zero()))
					lista.push_back(bl);
				bl.wykladnik.inkrementuj();
			}
			//TERAZ FRACTION
			pom=fraction.length();i=0;
			fraction=reverse(fraction);
			//bl.wykladnik.liczba=-1;
			bl.wykladnik.clear();
			bl.wykladnik.znak=1;
			//bl.wykladnik.clear();zmienic po polepszeniu prostych liczb
			//bl.wykladnik.znak=1;zmienic po polepszeniu prostych liczb
			while(1)
			{
				for(j=rozmiar_bloku-1;j>=0;--j)
				{
					if(i+8>=pom)
					{//cout<<"cwel\n\n";
						bl.clearBeginning(j-1);
						for(int l=i+8-pom;l>0;--l)
							fraction+=".";
						bl.parse( substr(fraction, i, i+8), j);
						if(!bl.zero())
							lista.push_front(bl);
						return;
					}
					bl.parse( substr(fraction, i, i+8), j);
					i+=8;
				}
				if(!bl.zero())
					lista.push_front(bl);
				bl.wykladnik.dekrementuj();//zmienic po polepszeniu prostych liczb
			}
		}
	}
	
	list<Blok>::iterator DuzaLiczba::getNextBlock(list<Blok>::iterator ite)//ite mustn't be lists end
	{
		list<Blok>::iterator i=ite+1;
		if(i!=lista.end()&&(((*ite).wykladnik.getIncremented()).compare( (*i/*(ite+1)*/).wykladnik )==rowny))
		{
			return ++ite;
		}
		Blok bl((*ite).wykladnik.getIncremented());
		return lista.insert(i, bl);
	}
	void DuzaLiczba::dodaj(list<Blok>::iterator ite, Blok &bl)// ite mustn't be lists end
	{//cout<<"DODAJE\n";
		if(!(*ite).dodaj(bl, 0))
			return;
		//Blok blo;
		ite=getNextBlock(ite);
		while((*ite).dodajJeden(0))//check test
		{
			//if((*ite).zero())
			//{	
				getNextBlock(ite);// creating next element in case ite was the last element in the list
				ite=lista.erase(ite);					
			//}
			//else
				//ite=getNextBlock(ite);
		}
		/*while((*ite).dodaj(blo, 1))
		{
			if((*ite).zero())
				lista.erase(ite);//check test
			ite=getNextBlock(ite);
		}*/
		return;
	}

	void DuzaLiczba::dodaj( DuzaLiczba dl )//adds the absolute values with no regard for the sign
	{
		if(dl.lista.empty())
			return;
		list<Blok>::iterator ite=lista.begin(), i=dl.lista.begin();
		Blok bl;
		Porownanie poro;
		for(i;i!=dl.lista.end();++i)
		{//cout<<"\n\n"<<((*ite).wykladnik.compare((*i).wykladnik))<<"\n\n";
		//cout<<"FOR\n";
			if((poro=(*ite).wykladnik.compare((*i).wykladnik))==wiekszy)//
			{//cout<<"OD RAZU \n";
				lista.insert(ite, *i);
				continue;
			}
			while(poro==mniejszy)//
			{
				if((++ite)==lista.end())
					break;
				poro=(*ite).wykladnik.compare((*i).wykladnik);//cout<<"PRZESUWAM\n";
			}
			if(ite!=lista.end())
			{
				if(poro==rowny)
				{//cout<<"ROWNY\n";
					dodaj(ite, *i);
				}
				else
				{//cout<<"WIEKSZY\n";
					lista.insert(ite, *i);
				}
			}
			else break;
		}
		for( i;i!=dl.lista.end();++i)
		{//cout<<"NA KONIEC DODAJE \n";
			lista.push_back(*i);//cout<<"FOR\n";
		}
	}
	
	DuzaLiczba DuzaLiczba::multiplyBlocks(Blok &a, Blok &b)
	{
		unsigned long long pom;
		int indeks;
		DuzaLiczba result;
		Blok bl(a.wykladnik+b.wykladnik), blo(bl);
		blo.wykladnik.inkrementuj();
		for(int i=0;i<rozmiar_bloku;++i)
			for(int j=0;j<rozmiar_bloku;++j)
			{//cout<<"CWEL "<<i<<" "<<j<<endl;
				indeks=i+j;
				pom=(unsigned long long)a.Pola[i]*b.Pola[j];//cout<<"POM TO: "<<pom<<endl;
				if(pom>MAX_UNSIGNED)
				{//cout<<"wiekszy od unsigned max  ";
					if(indeks+1<rozmiar_bloku)
					{//cout<<"indeks+1<rozmiar_bloku "<<pom%MAX_UNSIGNEDijeden<<" "<<pom/MAX_UNSIGNEDijeden<<endl;
						if(bl.dodajU(pom%MAX_UNSIGNEDijeden, indeks))
							blo.dodajJeden(0);
						if(bl.dodajU(pom/MAX_UNSIGNEDijeden, indeks+1))
							blo.dodajJeden(0);
					}
					else if(indeks+1>rozmiar_bloku)
					{//cout<<"indeks+1>rozmiar_bloku  "<<pom%MAX_UNSIGNEDijeden<<" "<<pom/MAX_UNSIGNEDijeden<<endl;
						blo.dodajU(pom%MAX_UNSIGNEDijeden, indeks-rozmiar_bloku);
						blo.dodajU(pom/MAX_UNSIGNEDijeden, indeks+1-rozmiar_bloku);
					}
					else
					{//cout<<"indeks+1==rozmiar_bloku  "<<pom%MAX_UNSIGNEDijeden<<" "<<pom/MAX_UNSIGNEDijeden<<endl;
						if(bl.dodajU(pom%MAX_UNSIGNEDijeden, indeks))
							blo.dodajJeden(0);
						blo.dodajU(pom/MAX_UNSIGNEDijeden, 0);
					}
				}
				else
				{//cout<<"mniejszy, rowny od unsigned max  ";
					if(indeks<rozmiar_bloku)
					{//cout<<"indeks<rozmiar_bloku  ";
						if(bl.dodajU(pom, indeks))
							blo.dodajJeden(0);
					}
					else
					{//cout<<"indeks>=rozmiar_bloku  ";
						blo.dodajU(pom, indeks-rozmiar_bloku);
					}
				}//cout<<endl;
			}//bl.display();blo.display();
		if(blo)
			result.lista.push_front(blo);
		if(bl)
			result.lista.push_front(bl);
		return result;
	}
	
	Porownanie DuzaLiczba::compareAbsolute(DuzaLiczba &dl)
	{
		Porownanie poro;
		for(list<Blok>::iterator thi=--(lista.end()), i=--(dl.lista.end());;--thi, --i)
		{
			if(thi==lista.end()||i==dl.lista.end())
			{
				if(thi==lista.end()&&i==dl.lista.end())
					return rowny;
				else if(thi==lista.end())
					return mniejszy;
				else
					return wiekszy;
			}
			if((poro=(*thi).compare((*i)))!=rowny)
				return poro;
		}
		return poro;
	}
	
	Porownanie DuzaLiczba::compare(DuzaLiczba &dl)//assuming the numbers are normalised
	{
		if(lista.empty()||dl.lista.empty())
		{
			if(lista.empty()&&dl.lista.empty())
				return rowny;
			else if(lista.empty())
			{
				if(dl.znak)
					return wiekszy;
				return mniejszy;
			}
			else
			{
				if(znak)
					return mniejszy;
				return wiekszy;
			}
		}
		
		if(znak^dl.znak)
		{
			if(znak)
				return mniejszy;
			else
				return wiekszy;
		}
		else
		{
			if(znak)
			{
				Porownanie poro=compareAbsolute(dl);
				if(poro==wiekszy)
					return mniejszy;
				if(poro==mniejszy)
					return wiekszy;
				return rowny;
			}
			else
				return compareAbsolute(dl);
		}
	}
	
	/*void DuzaLiczba::odejmij(DuzaLiczba &dl)
	{
		
	}*/
	
	void DuzaLiczba::odejmij(DuzaLiczba &dl) //substracts absolutes, first number's absolute value
	// has to be bigger or equal to the second's
	{
		list<Blok>::iterator thi=lista.begin(), i=dl.lista.begin();
		Porownanie poro;
		for(i;i!=dl.lista.end();++i)
		{
			poro=(*thi).wykladnik.compare((*i).wykladnik);
			while(poro==mniejszy)
			{//przewijam thi, żeby odjąć i od thi z tym samym wykladnikiem
				poro=(*(++thi)).wykladnik.compare((*i).wykladnik);//cout<<"PRZESUWAM\n";
			}
			if(poro==rowny)
			{//thi rowny wykladnik z aktualnym i
				switch((*thi).compare(*i))
				{
					case rowny: thi=lista.erase(thi);continue;
					case wiekszy: (*thi).odejmij(*i);break;
					case mniejszy:break;//todo
				}
				++thi;
			}
			else
			{//thi wiekszy wykladnik od aktualnego i, nie ma rownego
				Blok bl((*i).wykladnik.getIncremented(), MAX_UNSIGNED);
				musze dodawac elementy miedzy i, a thi z kolejnymi wykladnikami po i przed this
				raczej cos=thi, cos=lista.insert(cos, bl), dekrementuj b. wykladnik, while bl.wyk.dekreme()!=*i.wyk
			}
		}
	}
	DuzaLiczba DuzaLiczba::operator+( DuzaLiczba &dl )//	place the smaller number as the second argument
	{
		DuzaLiczba result;
		if(dl.zero())
			return result;
		if(compare(dl)>=1)
		{
			result=*this;
			if(znak^dl.znak)
			{
				result.odejmij(dl);
				return result;
			}
		}
		else
		{
			result=dl;
			if(znak^dl.znak)
			{
				result.odejmij(*this);
				return result;
			}		
		}
		result.dodaj(dl);
		return result;
	}
	
	DuzaLiczba DuzaLiczba::operator-( DuzaLiczba &dl )
	{//todo
		DuzaLiczba result;
		if(dl.zero())
			return result;
		if(compare(dl)>=1)
		{
			//result=*this;
			if(znak^dl.znak)
			{
				//result.odejmij(dl);
				//return result;
			}
		}
		else
		{
			//result=dl;
			if(znak^dl.znak)
			{
				//result.odejmij(*this);
				//return result;
			}		
		}
		result.odejmij(dl);
		return result;
	}
	
	DuzaLiczba DuzaLiczba::operator*( DuzaLiczba dl)
	{
		DuzaLiczba result;
		for(list<Blok>::iterator ite=lista.begin();ite!=lista.end();++ite)
			for(list<Blok>::iterator i=dl.lista.begin();i!=dl.lista.end();++i)
			{
				result.dodaj(multiplyBlocks(*ite, *i));
			}
		if(znak^dl.znak)
			result.znak=1;
		else
			result.znak=0;
		result.normalise();
		return result;
	}
	/*DuzaLiczba DuzaLiczba::operator^( DuzaLiczba &dl )
	{
		
		
	}*/
	