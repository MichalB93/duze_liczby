#ifndef DUZE_LICZBY_H
#define DUZE_LICZBY_H
#include "Blok.h"

typedef list<Blok>::iterator DL_iterator;
class DuzaLiczbaCount{public:
static unsigned long long ile;
DuzaLiczbaCount();//{++ile;}
~DuzaLiczbaCount();//{--ile;}
static unsigned long long getIle();
};



class DuzaLiczba: DuzaLiczbaCount{public:
	list<Blok> lista;
	bool znak;// 0 - dodatnia, 1 - ujemna;
	DuzaLiczba();
	DuzaLiczba(const DuzaLiczba &dl);

	DuzaLiczba(unsigned u);
	
	DuzaLiczba(int i);
	
	DuzaLiczba(unsigned long long ull);
	
	DuzaLiczba(double d);
	
	bool zero();
	
	bool testNormalisation();

	operator string();
	
	//operator double();

	void display();

	string displayBinarilly();
	
	string displayHexadecimally();
	
	void normalise();

	void parseFromString(const string &str);
	
	list<Blok>::iterator getNextBlock(list<Blok>::iterator ite);

	DL_iterator dodaj(list<Blok>::iterator ite, Blok &bl);

	void dodaj( DuzaLiczba dl);
	
	DuzaLiczba multiplyBlocks(Blok &a, Blok &b);
	
	Porownanie compareAbsolute(DuzaLiczba &dl);

	Porownanie compare(DuzaLiczba &dl);
	
	void fillSpaceBetweenItes(DL_iterator i, DL_iterator thi);
	
	void odejmij(DuzaLiczba &dl);
	
	DuzaLiczba operator+( DuzaLiczba &dl );
	
	DuzaLiczba operator-( DuzaLiczba &dl );
	
	DuzaLiczba operator*( DuzaLiczba dl );
	
	DuzaLiczba operator*( unsigned u );
	
	void parseFromDecimalString(const string &str);
};
#endif
