#ifndef PROSTA_LICZBA_PROW_H
#define PROSTA_LICZBA_PROW_H




class ProstaLiczbaCount{public:
	static unsigned long long ile;
	ProstaLiczbaCount();
	~ProstaLiczbaCount();
	static unsigned long long GetIle();
};


class ProstaLiczba: ProstaLiczbaCount{public:
	//bool znak;// 1-ujemny, 0-dodatni
	unsigned long long liczba;
	bool znak;
	
	static int ilecompare;
	static int ilegetincrementedabs;
	static int ileinkrementujabs;
	static int iledekrementujabs;

	~ProstaLiczba();
	
	ProstaLiczba();//:Pierwszy(new ProstyBlok), znak(0){}
	
	
	ProstaLiczba( long long b);//:Pierwszy(new ProstyBlok(b)), znak(a){}
	ProstaLiczba( const ProstaLiczba &ref);


	void display();

	void clear();

	Porownanie compare( const ProstaLiczba &ref);
	
	ProstaLiczba operator= ( const ProstaLiczba &ref);//NIEWYDAJNE GÓWNO

	ProstaLiczba operator+ ( const ProstaLiczba &ref);
	
	ProstaLiczba inkrementuj();

	ProstaLiczba getIncremented();

	ProstaLiczba dekrementuj();//dokonczyc
	
};
#endif