#include "pomoc.h"
#include "Prosta_Liczba_Nowa.h"

	inline ProstyBlok* rewind(ProstyBlok *wsk, TYP_SIZE_OF_LIST i)//takes an index, ranging from 0 to sizeoflist-1
	{
		for(;i>0;--i)
		{
			wsk=wsk->nast;
		}
		return wsk;
	}//rewind(Pierwszy, rozmiar-1) zwraca Ostatni

	ProstaLiczbaCount::ProstaLiczbaCount(){++ile;}
	ProstaLiczbaCount::~ProstaLiczbaCount(){--ile;}
	unsigned long long ProstaLiczbaCount::GetIle(){return ile;}
	unsigned long long ProstaLiczbaCount::ile=0;
	///////////////////////////////////////////////////
	
	unsigned ProstaLiczba::ilecompare=0;
	unsigned ProstaLiczba::ilegetincremented=0;
	unsigned ProstaLiczba::ileinkrementujabs=0;
	unsigned ProstaLiczba::iledekrementujabs=0;
	
	ProstaLiczba::~ProstaLiczba()
	{
		ProstyBlok *wsk;
		while(Pierwszy)
		{
			wsk=Pierwszy;
			Pierwszy=Pierwszy->nast;
			delete wsk;
		}
	}
	
	ProstaLiczba::ProstaLiczba():Pierwszy(new ProstyBlok), Ostatni(Pierwszy), znak(0), rozmiar(1){}
	
	ProstaLiczba::ProstaLiczba( bool a ):Pierwszy(new ProstyBlok), Ostatni(Pierwszy), znak(a), rozmiar(1){}
	
	ProstaLiczba::ProstaLiczba( unsigned long long b, bool a ):Pierwszy(new ProstyBlok(b)), Ostatni(Pierwszy), znak(a), rozmiar(1){}
	
	ProstaLiczba::ProstaLiczba( const ProstaLiczba &ref)
	{
		ProstyBlok *b=(ref.Pierwszy)->nast, *a;
		znak=ref.znak;
		rozmiar=ref.rozmiar;
		Pierwszy=new ProstyBlok(ref.Pierwszy);
		a=Pierwszy;
		while(b)
		{
			a->nast=new ProstyBlok(b);
			a=a->nast;b=b->nast;
		}
		Ostatni=a;//check test, ale raczej dobrze
	}
	/*void operator=(const ProstaLiczba &ref)
	{
		znak=ref.znak;
		Pierwszy->usun();
		Pierwszy=new ProstyBlok(ref.Pierwszy);
	}*/
	void ProstaLiczba::display()
	{
		ProstyBlok *wsk=Pierwszy;
		cout<<"Rozmiar: "<<rozmiar<<endl;
		while(wsk)
		{
			if(znak)
				cout<<"-";
			cout<<wsk->liczba<<" "<<wsk<<endl;
			wsk=wsk->nast;
		}
		cout<<endl;
	}	
	void ProstaLiczba::clear()// returns the number to it's default state, a zero
	{
		Pierwszy->liczba=0;
		rozmiar=1;
		ProstyBlok *wsk=Pierwszy->nast, *pom;
		while(wsk)
		{	
			pom=wsk;
			wsk=wsk->nast;
			delete pom;
		}
		Pierwszy->nast=NULL;
		Ostatni=Pierwszy;
	}
	Porownanie ProstaLiczba::compare( const ProstaLiczba &ref)
	{++ilecompare;
		if(znak^ref.znak)
		{
			if(znak)
				return mniejszy;
			return wiekszy;
		}
		if(rozmiar>ref.rozmiar)
			if(znak)
				return mniejszy;
			else
				return wiekszy;
		if(rozmiar<ref.rozmiar)
			if(znak)
				return wiekszy;
			else 
				return mniejszy;
		ProstyBlok *a=Pierwszy, *b=ref.Pierwszy;
		while(a)
		{
			if(a->liczba>b->liczba)
			{
				if(znak)
					return mniejszy;
				return wiekszy;
			}
			else if(a->liczba<b->liczba)
			{
				if(znak)
					return wiekszy;
				return mniejszy;
			}
			a=a->nast;b=b->nast;
		}
		return rowny;
		/*do
		{
			if(a->liczba<b->liczba)
				poprzedni=mniejszy;
			if(a->liczba>b->liczba)
				poprzedni=wiekszy;
			a=a->nast;b=b->nast;
		}
		while(a&&b);
		if(a)
		{
			if(znak)
				return mniejszy;
			return wiekszy;
		}
		else 
		{	
			if(b)
			{	
				if(znak)
					return wiekszy;
				return mniejszy;
			}
			else if(znak)
			{
				if(poprzedni==wiekszy)
					poprzedni=mniejszy;
				else if(poprzedni==mniejszy)
					poprzedni=wiekszy;
			}
				return poprzedni;
		}*/
	}
	
	ProstaLiczba ProstaLiczba::operator= ( const ProstaLiczba &ref)//NIEWYDAJNE GÓWNO
	{//test check
		znak=ref.znak;
		clear();
		ProstyBlok *b=(ref.Pierwszy)->nast, *a;
		rozmiar=ref.rozmiar;
		Pierwszy->liczba=ref.Pierwszy->liczba;
		a=Pierwszy;
		while(b)
		{
			a->nast=new ProstyBlok(b);
			a=a->nast;b=b->nast;
		}
		Ostatni=a;//check test, ale raczej dobrze
		return *this;
	}
	void ProstaLiczba::inkrementujAbsolute()
	{
		++ileinkrementujabs;
		if(Ostatni->liczba!=MAX_TYP)
		{
			++(Ostatni->liczba);
			return;
		}
		Ostatni->liczba=0;
		ProstyBlok *wsk;
		for(int i=rozmiar-2;i>=0;--i)
		{
			if((wsk=rewind(Pierwszy, i))->liczba==MAX_TYP)
			{
				wsk->liczba=0;
			}
			else
			{
				++(wsk->liczba);
				return;
			}
		}
		++rozmiar;
		wsk=Pierwszy;
		Pierwszy=new ProstyBlok;
		Pierwszy->nast=wsk;
	}
	void ProstaLiczba::dekrementujAbsolute()
	{// dziala zdaje sie
		++iledekrementujabs;
		if(Ostatni->liczba)
		{
			--(Ostatni->liczba);
			return;
		}
		//rewind(Pierwszy, rozmiar-1) zwraca Ostatni
		Ostatni->liczba=MAX_TYP;
		ProstyBlok *wsk;
		for(int i=rozmiar-2;i>=0;--i)
		{
			if((wsk=rewind(Pierwszy, i))->liczba)
			{
				--(wsk->liczba);
				return;
			}
			else
			{
				(wsk->liczba)=MAX_TYP;
			}
		}
		if(rozmiar==1)
		{
			znak=!znak;
			Pierwszy->liczba=0;
		}
		else
		{
			--rozmiar;
			wsk=Pierwszy->nast;
			delete Pierwszy;
			Pierwszy=wsk;
		}
	}
	ProstaLiczba ProstaLiczba::inkrementuj()
	{
		if(znak)
			dekrementujAbsolute();
		else
			inkrementujAbsolute();
		return *this;
	}
	ProstaLiczba ProstaLiczba::getIncremented()
	{
		++ilegetincremented;
		ProstaLiczba l(*this);
		l.inkrementuj();
		return l;
	}
	ProstaLiczba ProstaLiczba::dekrementuj()//dokonczyc
	{
		if(znak)
			inkrementujAbsolute();
		else
			dekrementujAbsolute();
		/*ProstyBlok *wsk=Pierwszy;
		if(wsk->liczba)
		{
			--(wsk->liczba);
			return *this;
		}
		wsk->liczba=MAX_UNSIGNED_LONG;
		ProstyBlok *pom=wsk->nast;
		if(!pom)
			return *this;
		while(wsk->nast)
		{
			if(wsk->liczba)
			{
				--(wsk->liczba);
				return *this;
			}
			wsk->liczba=MAX_UNSIGNED_LONG;
			if(wsk=wsk->nast)
				break;
			pom=wsk;
			
		}*/
		return *this;
	}
	
	ProstaLiczba ProstaLiczba::operator+ ( const ProstaLiczba &ref )
	{//todo
		ProstaLiczba result(*this);//todotodotodo
		result.Pierwszy->liczba+=ref.Pierwszy->liczba;//todotodotodo
		return result;
	}