#ifndef PROSTA_LICZBA_NOWA_H
#define PROSTA_LICZBA_NOWA_H
#include "ProstyBlok.h"
typedef unsigned TYP_SIZE_OF_LIST;
#define MAX_TYP_SIZE_OF_LIST 4294967295

	inline ProstyBlok* rewind(TYP_SIZE_OF_LIST i);//takes an index, ranging from 0 to sizeoflist-1
	
class ProstaLiczbaCount{public:
	static unsigned long long ile;
	ProstaLiczbaCount();
	~ProstaLiczbaCount();
	static unsigned long long GetIle();
};


class ProstaLiczba: ProstaLiczbaCount{public:
	bool znak;// 1-ujemny, 0-dodatni
	ProstyBlok *Pierwszy, *Ostatni;
	TYP_SIZE_OF_LIST rozmiar;
	
	static unsigned ilecompare;
	static unsigned ilegetincremented;
	static unsigned ileinkrementujabs;
	static unsigned iledekrementujabs;
	~ProstaLiczba();
	
	ProstaLiczba();//:Pierwszy(new ProstyBlok), znak(0){}
	
	ProstaLiczba( bool a );//:Pierwszy(new ProstyBlok), znak(a){}
	
	ProstaLiczba( unsigned long long b, bool a );//:Pierwszy(new ProstyBlok(b)), znak(a){}
	
	ProstaLiczba( const ProstaLiczba &ref);

	void display();

	void clear();

	Porownanie compare( const ProstaLiczba &ref);//sprawdzic
	
	ProstaLiczba operator= ( const ProstaLiczba &ref );//NIEWYDAJNE GÓWNO
	
	void inkrementujAbsolute();
	
	void dekrementujAbsolute();

	ProstaLiczba inkrementuj();

	ProstaLiczba getIncremented();
	
	ProstaLiczba dekrementuj();
	
	ProstaLiczba operator+ ( const ProstaLiczba &ref );
	
};
#endif