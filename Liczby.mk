##
## Auto Generated makefile by CodeLite IDE
## any manual changes will be erased      
##
## Debug
ProjectName            :=Liczby
ConfigurationName      :=Debug
WorkspacePath          := "C:\Projekty\Graf"
ProjectPath            := "C:\Projekty\Liczby"
IntermediateDirectory  :=./Debug
OutDir                 := $(IntermediateDirectory)
CurrentFileName        :=
CurrentFilePath        :=
CurrentFileFullPath    :=
User                   :=michal
Date                   :=09/16/14
CodeLitePath           :="C:\Program Files (x86)\CodeLite"
LinkerName             :=g++
SharedObjectLinkerName :=g++ -shared -fPIC
ObjectSuffix           :=.o
DependSuffix           :=.o.d
PreprocessSuffix       :=.o.i
DebugSwitch            :=-gstab
IncludeSwitch          :=-I
LibrarySwitch          :=-l
OutputSwitch           :=-o 
LibraryPathSwitch      :=-L
PreprocessorSwitch     :=-D
SourceSwitch           :=-c 
OutputFile             :=$(IntermediateDirectory)/$(ProjectName)
Preprocessors          :=
ObjectSwitch           :=-o 
ArchiveOutputSwitch    := 
PreprocessOnlySwitch   :=-E 
ObjectsFileList        :="Liczby.txt"
PCHCompileFlags        :=
MakeDirCommand         :=makedir
RcCmpOptions           := 
RcCompilerName         :=windres
LinkOptions            :=  
IncludePath            :=  $(IncludeSwitch). $(IncludeSwitch). 
IncludePCH             := 
RcIncludePath          := 
Libs                   := 
ArLibs                 :=  
LibPath                := $(LibraryPathSwitch). 

##
## Common variables
## AR, CXX, CC, AS, CXXFLAGS and CFLAGS can be overriden using an environment variables
##
AR       := ar rcus
CXX      := g++
CC       := gcc
CXXFLAGS :=  -g -O0 -Wall $(Preprocessors)
CFLAGS   :=  -g -O0 -Wall $(Preprocessors)
ASFLAGS  := 
AS       := as


##
## User defined environment variables
##
CodeLiteDir:=C:\Program Files (x86)\CodeLite
UNIT_TEST_PP_SRC_DIR:=C:\UnitTest++-1.3
Objects0=$(IntermediateDirectory)/Duze_Liczby$(ObjectSuffix) $(IntermediateDirectory)/main$(ObjectSuffix) $(IntermediateDirectory)/pomoc$(ObjectSuffix) $(IntermediateDirectory)/Blok$(ObjectSuffix) $(IntermediateDirectory)/ProstyBlok$(ObjectSuffix) $(IntermediateDirectory)/ProstaLiczbaProw$(ObjectSuffix) 



Objects=$(Objects0) 

##
## Main Build Targets 
##
.PHONY: all clean PreBuild PrePreBuild PostBuild
all: $(OutputFile)

$(OutputFile): $(IntermediateDirectory)/.d $(Objects) 
	@$(MakeDirCommand) $(@D)
	@echo "" > $(IntermediateDirectory)/.d
	@echo $(Objects0)  > $(ObjectsFileList)
	$(LinkerName) $(OutputSwitch)$(OutputFile) @$(ObjectsFileList) $(LibPath) $(Libs) $(LinkOptions)

$(IntermediateDirectory)/.d:
	@$(MakeDirCommand) "./Debug"

PreBuild:


##
## Objects
##
$(IntermediateDirectory)/Duze_Liczby$(ObjectSuffix): Duze_Liczby.cpp $(IntermediateDirectory)/Duze_Liczby$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "C:/Projekty/Liczby/Duze_Liczby.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/Duze_Liczby$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/Duze_Liczby$(DependSuffix): Duze_Liczby.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/Duze_Liczby$(ObjectSuffix) -MF$(IntermediateDirectory)/Duze_Liczby$(DependSuffix) -MM "Duze_Liczby.cpp"

$(IntermediateDirectory)/Duze_Liczby$(PreprocessSuffix): Duze_Liczby.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/Duze_Liczby$(PreprocessSuffix) "Duze_Liczby.cpp"

$(IntermediateDirectory)/main$(ObjectSuffix): main.cpp $(IntermediateDirectory)/main$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "C:/Projekty/Liczby/main.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/main$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/main$(DependSuffix): main.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/main$(ObjectSuffix) -MF$(IntermediateDirectory)/main$(DependSuffix) -MM "main.cpp"

$(IntermediateDirectory)/main$(PreprocessSuffix): main.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/main$(PreprocessSuffix) "main.cpp"

$(IntermediateDirectory)/pomoc$(ObjectSuffix): pomoc.cpp $(IntermediateDirectory)/pomoc$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "C:/Projekty/Liczby/pomoc.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/pomoc$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/pomoc$(DependSuffix): pomoc.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/pomoc$(ObjectSuffix) -MF$(IntermediateDirectory)/pomoc$(DependSuffix) -MM "pomoc.cpp"

$(IntermediateDirectory)/pomoc$(PreprocessSuffix): pomoc.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/pomoc$(PreprocessSuffix) "pomoc.cpp"

$(IntermediateDirectory)/Blok$(ObjectSuffix): Blok.cpp $(IntermediateDirectory)/Blok$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "C:/Projekty/Liczby/Blok.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/Blok$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/Blok$(DependSuffix): Blok.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/Blok$(ObjectSuffix) -MF$(IntermediateDirectory)/Blok$(DependSuffix) -MM "Blok.cpp"

$(IntermediateDirectory)/Blok$(PreprocessSuffix): Blok.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/Blok$(PreprocessSuffix) "Blok.cpp"

$(IntermediateDirectory)/ProstyBlok$(ObjectSuffix): ProstyBlok.cpp $(IntermediateDirectory)/ProstyBlok$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "C:/Projekty/Liczby/ProstyBlok.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/ProstyBlok$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/ProstyBlok$(DependSuffix): ProstyBlok.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/ProstyBlok$(ObjectSuffix) -MF$(IntermediateDirectory)/ProstyBlok$(DependSuffix) -MM "ProstyBlok.cpp"

$(IntermediateDirectory)/ProstyBlok$(PreprocessSuffix): ProstyBlok.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/ProstyBlok$(PreprocessSuffix) "ProstyBlok.cpp"

$(IntermediateDirectory)/ProstaLiczbaProw$(ObjectSuffix): ProstaLiczbaProw.cpp $(IntermediateDirectory)/ProstaLiczbaProw$(DependSuffix)
	$(CXX) $(IncludePCH) $(SourceSwitch) "C:/Projekty/Liczby/ProstaLiczbaProw.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/ProstaLiczbaProw$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/ProstaLiczbaProw$(DependSuffix): ProstaLiczbaProw.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/ProstaLiczbaProw$(ObjectSuffix) -MF$(IntermediateDirectory)/ProstaLiczbaProw$(DependSuffix) -MM "ProstaLiczbaProw.cpp"

$(IntermediateDirectory)/ProstaLiczbaProw$(PreprocessSuffix): ProstaLiczbaProw.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/ProstaLiczbaProw$(PreprocessSuffix) "ProstaLiczbaProw.cpp"


-include $(IntermediateDirectory)/*$(DependSuffix)
##
## Clean
##
clean:
	$(RM) $(IntermediateDirectory)/Duze_Liczby$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/Duze_Liczby$(DependSuffix)
	$(RM) $(IntermediateDirectory)/Duze_Liczby$(PreprocessSuffix)
	$(RM) $(IntermediateDirectory)/main$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/main$(DependSuffix)
	$(RM) $(IntermediateDirectory)/main$(PreprocessSuffix)
	$(RM) $(IntermediateDirectory)/pomoc$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/pomoc$(DependSuffix)
	$(RM) $(IntermediateDirectory)/pomoc$(PreprocessSuffix)
	$(RM) $(IntermediateDirectory)/Blok$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/Blok$(DependSuffix)
	$(RM) $(IntermediateDirectory)/Blok$(PreprocessSuffix)
	$(RM) $(IntermediateDirectory)/ProstyBlok$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/ProstyBlok$(DependSuffix)
	$(RM) $(IntermediateDirectory)/ProstyBlok$(PreprocessSuffix)
	$(RM) $(IntermediateDirectory)/ProstaLiczbaProw$(ObjectSuffix)
	$(RM) $(IntermediateDirectory)/ProstaLiczbaProw$(DependSuffix)
	$(RM) $(IntermediateDirectory)/ProstaLiczbaProw$(PreprocessSuffix)
	$(RM) $(OutputFile)
	$(RM) $(OutputFile).exe
	$(RM) "../graf/.build-debug/Liczby"


