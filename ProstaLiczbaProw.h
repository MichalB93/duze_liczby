#ifndef PROSTA_LICZBA_PROW_H
#define PROSTA_LICZBA_PROW_H




class ProstaLiczbaCount{public:
	static unsigned long long ile;
	ProstaLiczbaCount();
	~ProstaLiczbaCount();
	static unsigned long long GetIle();
};


class ProstaLiczba: ProstaLiczbaCount{public:
	//bool znak;// 1-ujemny, 0-dodatni
	long long liczba;
	
	static int ilecompare;
	static int ilegetincrementedabs;
	static int ileinkrementujabs;
	static int iledekrementujabs;

	~ProstaLiczba();
	
	ProstaLiczba();//:Pierwszy(new ProstyBlok), znak(0){}
	
	
	ProstaLiczba( long long b);//:Pierwszy(new ProstyBlok(b)), znak(a){}
	ProstaLiczba( const ProstaLiczba &ref);


	void display();

	void clear();

	Porownanie compare( const ProstaLiczba &ref);
	
	ProstaLiczba operator= ( const ProstaLiczba &ref);

	ProstaLiczba operator+ ( const ProstaLiczba &ref);
	
	void inkrementuj();

	ProstaLiczba getIncremented();

	void dekrementuj();//dokonczyc
	
};
#endif